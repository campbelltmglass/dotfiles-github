### GENERAL SETTINGS ###
# Default Editor
export EDITOR='vim'
export VISUAL='vim'


### USEFUL ALIASES ###
# ls family
alias ls='ls --color=auto'
alias la='ls -a --color=auto'

# tmux family
alias newmux='tmux new-session -s'
alias attachmux='tmux attach -t'
alias detachmux='tmux detach'
alias listmux='tmux list-sessions'
alias movemux='tmux move-window -t'
shiftmux () { tmux move-window -s $1 -t $2; }
swapmux () { # Swaps window numbering of two args in tmux
  SWAP_WINDOW=100;
  shiftmux $1 $SWAP_WINDOW;
  shiftmux $2 $1;
  shiftmux $SWAP_WINDOW $2;
  unset SWAP_WINDOW;
}

# .zshrc family
alias zshrc='vim ~/.zshrc'
alias rezshrc='source ~/.zshrc'


### GIT SETTINGS ###
# Setup autocomplete for git branches on Mac
autoload -Uz compinit && compinit


### COLOR SETTINGS ###
# Colorize and customize the PROMPT
# from: https://scriptingosx.com/2019/07/moving-to-zsh-06-customizing-the-zsh-prompt/
PROMPT='%(?.%F{green}√.%F{red}?%?)%f %B%F{magenta}%1~%f%b %# '
